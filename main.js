var tw = new TimelineMax(
    // {onComplete:restart}
);
var tot = document.querySelectorAll('.kotak').length;
var bol = false;
var hov_gal = document.getElementById("logo-2").addEventListener("mouseover", hover_galery);

function play() {    
    var name = "stick";
    tw.to("#stick-1" ,.25, {rotation:0, y:"+=64",x:"-=18", repeat:1, yoyo:true, ease:Sine.easeInOut, force3D:false }, "kotak1");
    tw.to("#stick-2" ,.25, {rotation:45, y:"+=23", x:"-=63", repeat:1, yoyo:true, ease:Sine.easeInOut, force3D:false }, "kotak1");
    tw.to("#stick-3" ,.25, {rotation:90, y:"-=15", x:"-=63", repeat:1, yoyo:true, ease:Sine.easeInOut, force3D:false }, "kotak1");
    tw.to("#stick-4" ,.25, {rotation:135, y:"-=63", x:"-=21", repeat:1, yoyo:true, ease:Sine.easeInOut, force3D:false }, "kotak1");
    tw.to("#stick-5" ,.25, {rotation:-180, y:"-=63", x:"+=25", repeat:1, yoyo:true, ease:Sine.easeInOut, force3D:false }, "kotak1");
    tw.to("#stick-6" ,.25, {rotation:-135, y:"-=25", x:"+=63", repeat:1, yoyo:true, ease:Sine.easeInOut, force3D:false }, "kotak1");
    tw.to("#stick-7" ,.25, {rotation:-90, y:"+=26", x:"+=63", repeat:1, yoyo:true, ease:Sine.easeInOut, force3D:false }, "kotak1");
    tw.to("#stick-8" ,.25, {rotation:-45, y:"+=63", x:"+=26", repeat:1, yoyo:true, ease:Sine.easeInOut, force3D:false }, "kotak1");
    tw.to("#cam-circle" ,.25, {rotation:170, repeat:1, yoyo:true, ease:Sine.easeInOut, force3D:false }, "kotak1");
    // tw.to("#cam-circle" ,.5, {rotation:170, repeat:1, yoyo:true, onStart:disp, onStartParams:[name], ease:Sine.easeInOut, force3D:false }, "kotak1");
    play2();
}

function play2() {
    tw.from("#logo" ,0.8, {opacity:0, onStart:disp, onStartParams:['logo'], ease:Sine.easeInOut},"kotak2");
    // tw.from("#logo-2" ,0.8, {opacity:0, onStart:disp, onStartParams:['logo-2'], ease:Sine.easeInOut},"kotak2");
}

function disp(name) {
    var nm = document.getElementById(name);
    nm.style.display = "block";
}

function restart(){
    tw.restart();
}

function hover_galery() {
    var production = document.getElementById("production");
    var open = document.getElementById("open");
    TweenLite.to(production, 1, {opacity:0});
    TweenLite.from(open, 1, {delay:"1", opacity:0,onStart:disp,onStartParams:['open'], ease:Sine.easeInOut});
}